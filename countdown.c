///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo

// @author Creel Patrocinio <creel@hawaii.edu>
// @date   02/24/2022
///////////////////////////////////////////////////////////////////////////////


int main() {

   return 0;
}
